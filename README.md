# NVIM

Based on LazyVim with my customization

## Installation

1. Clone this repository to `~/.config/nvim`:
2. Open `nvim` and it should automatically install all plugins
