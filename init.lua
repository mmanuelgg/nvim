-- bootstrap lazy.nvim, LazyVim and your plugins
if vim.g.vscode then
  require("vscode-config.keymaps")
else
  require("config.lazy")
  require("after.dap")
  require("custom")
end
