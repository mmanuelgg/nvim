local dap = require("dap")
dap.configurations.java = {
  {
    type = "java",
    name = "TS Application",
    request = "launch",
    mainClass = "kpiengine.Application",
    projectName = "tupl-streams-service",
    env = {
      GRID_FILES_PATH = "./tmp/grids",
      EXECUTABLE_FILES_FOLDER = "./external-utils",
      PYTHON_INTERPRETER_PATH = "python3",
      JFR_FILES_PATH = "./tmp/jfr",
      HBASE_QUORUM = "127.0.0.1",
      BIGTABLE_EMULATOR_HOST = "localhost:8086",
    },
  },
  {
    type = "java",
    request = "attach",
    name = "Debug (Attach) - Remote",
    hostName = "localhost",
    port = 50390,
  },
  {
    type = "java",
    request = "launch",
    name = "Debug (Launch) - Remote",
    hostName = "localhost",
    port = 50390,
    console = "internalConsole",
    stopOnEntry = true,
    pathMappings = {
      {
        localRoot = "${workspaceFolder}",
        remoteRoot = ".",
      },
    },
  },
  {
    type = "java",
    request = "launch",
    name = "Debug (Launch) - Local",
    console = "internalConsole",
    mainClass = "${file}",
  },
}

-- Supposedly not needed with nvim-jdtls
-- dap.adapters.java = function(callback)
--   callback({ type = "server", host = "localhost", port = 8080 })
-- end

dap.configurations.python = {
  {
    type = "python",
    request = "launch",
    name = "Run app",
    cwd = "${workspaceFolder}",
    program = "./app/run.py",
    env = {
      CONFIG_FILENAME = "./app/resources/config/application.conf",
      -- MONGO_HOST = "mongodb",
      MONGO_HOST = "localhost",
      MONGO_PORT = "27017",
      MONGO_DATABASE = "orchestrator",
      DB_USER = "",
      MONGO_ROOT_USER = "root",
    },
  },
  {
    type = "python",
    request = "launch",
    name = "Python: Current File",
    program = "${file}",
    pythonPath = function()
      return "/usr/bin/python3"
    end,
  },
  {
    type = "python",
    request = "launch",
    name = "Python: Attach",
    connect = {
      type = "tcp",
      host = "localhost",
      port = 5678,
    },
  },
}

-- JavaScript
if not dap.adapters["pwa-node"] then
  require("dap").adapters["pwa-node"] = {
    type = "server",
    host = "localhost",
    port = "${port}",
    executable = {
      command = "node",
      args = {
        LazyVim.get_pkg_path("js-debug-adapter", "/js-debug/src/dapDebugServer.js"),
        "${port}",
      },
    },
  }
end
if not dap.adapters["node"] then
  dap.adapters["node"] = function(cb, config)
    if config.type == "node" then
      config.type = "pwa-node"
    end
    local nativeAdapter = dap.adapters["pwa-node"]
    if type(nativeAdapter) == "function" then
      nativeAdapter(cb, config)
    else
      cb(nativeAdapter)
    end
  end
end

local js_filetypes = { "typescript", "javascript", "typescriptreact", "javascriptreact" }

local vscode = require("dap.ext.vscode")
vscode.type_to_filetypes["node"] = js_filetypes
vscode.type_to_filetypes["pwa-node"] = js_filetypes

for _, language in ipairs(js_filetypes) do
  if not dap.configurations[language] then
    dap.configurations[language] = {
      {
        type = "pwa-node",
        request = "launch",
        name = "Launch file",
        program = "${file}",
        cwd = "${workspaceFolder}",
        skipFiles = { "<node_internals>/**" },
      },
      {
        type = "pwa-node",
        request = "attach",
        name = "Attach",
        processId = require("dap.utils").pick_process,
        cwd = "${workspaceFolder}",
        skipFiles = { "<node_internals>/**" },
      },
    }
  end
end
