-- Autocmds are automatically loaded on the VeryLazy event
-- Default autocmds that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/autocmds.lua
-- Add any additional autocmds here

--vim.cmd.hi("Normal guibg=NONE ctermbg=NONE")

vim.api.nvim_create_autocmd({ "ColorScheme", "BufEnter" }, {
  callback = function()
    local current_colorscheme = vim.g.colors_name
    if current_colorscheme == "doom-one" then
      vim.api.nvim_set_hl(0, "LineNr", { fg = "#BD93F9" })
      vim.api.nvim_set_hl(0, "CursorLineNr", { fg = "#FFB86C" })
    end
  end,
})

vim.api.nvim_create_autocmd("FileType", {
  pattern = "java",
  callback = function()
    -- vim.api.nvim_buf_set_option(0, "expandtab", true)
    vim.opt_local.shiftwidth = 4
    vim.opt_local.tabstop = 4
  end,
})
