-- Keymaps are automatically loaded on the VeryLazy event
-- Default keymaps that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/keymaps.lua
-- Add any additional keymaps here

local keymap = vim.keymap

keymap.set("i", "jk", "<ESC>", { desc = "Remap for ESC in insert mode" })

-- next greatest remap ever : asbjornHaland
keymap.set("n", "<leader>y", '"+y', { desc = "Copy to clipboard" })
keymap.set("v", "<leader>y", '"+y', { desc = "Copy to clipboard" })
keymap.set("n", "<leader>Y", '"+Y', { desc = "Copy full line to clipboard" })

keymap.set("n", "Q", "<nop>", { desc = "Disable Ex mode" })

keymap.set("n", "<C-k>", "<CMD>cnext<CR>zz", { desc = "Go to next quickfix" })
keymap.set("n", "<C-j>", "<CMD>cprev<CR>zz", { desc = "Go to previous quickfix" })
keymap.set("n", "<leader>k", "<CMD>lnext<CR>zz", { desc = "Go to next location list" })
keymap.set("n", "<leader>j", "<CMD>lprev<CR>zz", { desc = "Go to previous location list" })

keymap.set("n", "<leader>fx", "<CMD>!chmod +x %<CR>", { silent = true, desc = "Set this file as executable" })

-- windows remap
keymap.set("n", "<leader>ww", "<C-W>w", { desc = "Move to next window", remap = true })
keymap.set("n", "<leader>wj", "<C-W>j", { desc = "Move to window below", remap = true })
keymap.set("n", "<leader>wk", "<C-W>k", { desc = "Move to window above", remap = true })
keymap.set("n", "<leader>wl", "<C-W>l", { desc = "Move to window on the right", remap = true })
keymap.set("n", "<leader>wh", "<C-W>h", { desc = "Move to window on the left", remap = true })
keymap.set("n", "<leader>wv", "<C-W>v", { desc = "Split window vertical", remap = true })

-- terminal
-- vim.keymap.set("t", "<Esc>", "<C-\\><C-n>", { desc = "Exit terminal mode" })
keymap.set("t", "jk", "<C-\\><C-n>", { desc = "Exit terminal mode" })
keymap.set("t", "<C-h>", "<C-\\><C-n><C-W>h", { desc = "Move to window on the left" })
keymap.set("t", "<C-j>", "<C-\\><C-n><C-W>j", { desc = "Move to window below" })
keymap.set("t", "<C-k>", "<C-\\><C-n><C-W>k", { desc = "Move to window above" })
keymap.set("t", "<C-l>", "<C-\\><C-n><C-W>l", { desc = "Move to window on the right" })

-- Persistence
keymap.set(
  "n",
  "<leader>pl",
  '<CMD>lua require("persistence").load({last = true})<CR>',
  { desc = "Restore last session" }
)
keymap.set(
  "n",
  "<leader>pd",
  '<CMD>lua require("persistence").load()<CR>',
  { desc = "Restore current directory session" }
)

keymap.set("n", "<leader>dP", "", { desc = "Python" })

-- keymap.set("n", "<leader>pv", vim.cmd.Ex, { desc = "Visualize directory" })
