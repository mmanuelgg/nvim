local curl = require("plenary.curl")
local json = vim.fn.json_decode

local function load_dotenv(dotenv_file)
  local f = io.open(dotenv_file, "r")
  if not f then
    error("Failed to open .env file: " .. dotenv_file)
  end
  for line in f:lines() do
    local key, value = line:match("^([^=]+)=(.*)$")
    if key then
      vim.fn.setenv(key, value)
    end
  end
  f:close()
end

local function get_jira_credentials()
  load_dotenv(vim.fn.stdpath("config") .. "/.env")
  return {
    url = "https://" .. vim.fn.getenv("JIRA_WEB") .. "/rest/api/2/search",
    auth = vim.fn.getenv("JIRA_USER") .. ":" .. vim.fn.getenv("JIRA_TOKEN"),
  }
end

local function fetch_jira_data(jql)
  local creds = get_jira_credentials()
  local response = curl.get(creds.url, {
    auth = creds.auth,
    headers = {
      ["Content-Type"] = "application/json",
    },
    query = {
      jql = jql,
      fields = "key,summary,status,description,reporter,priority",
    },
  })

  if response.status == 200 then
    local data = json(response.body)
    local issues = {}
    for _, issue in ipairs(data.issues) do
      table.insert(issues, {
        text = (issue.key .. ": " .. issue.fields.summary) or "",
        key = issue.key or "",
        summary = issue.fields.summary or "",
        status = issue.fields.status.name or "",
        description = issue.fields.description or "",
        reporter = issue.fields.reporter.displayName or "",
        priority = issue.fields.priority.name or "",
      })
    end
    return issues
  else
    error("Failed to fetch JIRA issues: " .. response.status)
  end
end

local function fetch_jira_issues()
  return fetch_jira_data('assignee=currentUser() OR key IN ("DEL-864","TOS-178","TOS-181")')
end

local function fetch_jira_sprint()
  return fetch_jira_data("assignee=currentUser() AND sprint in openSprints()")
end

local function fetch_jira_watched_issues()
  return fetch_jira_data("watcher=currentUser()")
end

local function fetch_jira_reported_issues()
  return fetch_jira_data("reporter=currentUser()")
end

local function fetch_jira_custom()
  local query = vim.fn.input("Enter JIRA JQL query: ")
  return fetch_jira_data(tostring(query))
end

local function safe_string(value)
  return type(value) == "string" and value or ""
end

local function preview_issue(ctx)
  ctx.preview:set_title("Issue")
  local fields = { "key", "summary", "status", "reporter", "priority", "description" }
  local lines = {}

  for _, field in ipairs(fields) do
    table.insert(lines, string.format("[%s]\n%s\n", field:gsub("^%l", string.upper), safe_string(ctx.item[field])))
  end

  ctx.preview:set_lines(lines)
end

local function setup_picker(key, source, finder, format, confirm, actions, preview)
  vim.keymap.set("n", key, function()
    Snacks.picker.pick({
      source = source,
      finder = finder,
      format = format,
      confirm = confirm,
      win = {
        input = {
          keys = {
            ["<C-y>"] = { "log", mode = "i" },
            ["<C-e>"] = { "sprint_log", mode = "i" },
          },
        },
      },
      actions = actions,
      preview = preview,
    })
  end, { desc = source })
end

local confirm = function(picker, item)
  picker:close()
  local formatted_text = { string.format("%s %s", item.key, item.summary) }
  vim.api.nvim_put(formatted_text, "l", true, true)
end

local actions = {
  ["log"] = function(picker, item)
    picker:close()
    local lines = {
      string.format("### `%s` `%s`", item.key, item.summary),
      "",
      "> `PLANNED` `HH:MM > HH:MM`",
      "> - Comment",
    }
    vim.api.nvim_put(lines, "l", true, true)
  end,
  ["sprint_log"] = function(picker, item)
    picker:close()
    local lines = { string.format("## %s %s", item.key, item.summary) }
    vim.api.nvim_put(lines, "l", true, true)
  end,
}

vim.keymap.set("n", "<leader>J", "", { desc = "JIRA" })
setup_picker("<leader>Ji", "JIRA Issues", fetch_jira_issues, "text", confirm, actions, preview_issue)
setup_picker("<leader>Js", "JIRA Open Sprints Issues", fetch_jira_sprint, "text", confirm, actions, preview_issue)
setup_picker("<leader>Jw", "JIRA Watched Issues", fetch_jira_watched_issues, "text", confirm, actions, preview_issue)
setup_picker("<leader>Jr", "JIRA Reported Issues", fetch_jira_reported_issues, "text", confirm, actions, preview_issue)
setup_picker("<leader>Jc", "JIRA Custom Query", fetch_jira_custom, "text", confirm, actions, preview_issue)
