local function notebook_utils()
  local M = {}

  local nb_dir = "$HOME/.nb/"
  local current_notebook = "work" -- Default notebook
  local work_notes = vim.fn.expand("$HOME/work-notes/")

  -- Function to get the list of notebooks dynamically, excluding dotfiles
  function M.get_notebooks()
    local notebooks = {}
    local dir = vim.fn.expand(nb_dir)

    for _, notebook in ipairs(vim.fn.readdir(dir)) do
      if vim.fn.isdirectory(dir .. notebook) and not notebook:match("^%.") then
        table.insert(notebooks, notebook)
      end
    end

    return notebooks
  end

  -- Function to change the notebook using Telescope
  function M.set_notebook()
    local notebooks = M.get_notebooks()

    if not notebooks or #notebooks == 0 then
      print("No notebooks available.")
      return
    end

    local choices = vim.tbl_map(function(notebook)
      return notebook
    end, notebooks)

    require("telescope.pickers")
      .new({}, {
        prompt_title = "Select a Notebook",
        finder = require("telescope.finders").new_table({
          results = choices,
          entry_maker = function(entry)
            return {
              value = entry,
              display = entry,
              ordinal = entry,
            }
          end,
        }),
        sorter = require("telescope.sorters").get_generic_fuzzy_sorter(),
        attach_mappings = function(_, map)
          map("i", "<CR>", function(prompt_bufnr)
            local selection = require("telescope.actions.state").get_selected_entry()
            if selection then
              current_notebook = selection.value
              print("Notebook changed to: " .. current_notebook)
            end
            require("telescope.actions").close(prompt_bufnr)
          end)
          return true
        end,
      })
      :find()
  end

  -- Function to open a journal entry, creating it if it doesn't exist
  function M.open_journal_entry(offset)
    local current_timestamp = os.time() + (offset or 0) * 86400
    local current_date = os.date("*t", current_timestamp)
    local new_date = string.format("%d-%02d-%02d", current_date.year, current_date.month, current_date.day)
    local dir = string.format(nb_dir .. current_notebook .. "/Journal/%d/%02d/", current_date.year, current_date.month)
    local expanded_dir = vim.fn.expand(dir)

    os.execute("mkdir -p " .. expanded_dir)

    local file_path = expanded_dir .. new_date .. ".md"
    local title = os.date("%A, %B %d, %Y", current_timestamp)

    vim.cmd("edit " .. file_path)

    if vim.fn.line("$") == 1 then
      local lines = {
        "---",
        "title: '" .. title .. "'",
        "date: " .. new_date,
        "tags: ['daily']",
        "---",
        "",
        "#unlogged",
        "",
        "#daily",
        "",
        "# " .. new_date,
        "",
        "```markdown",
        "Planned for today:",
        "",
        "JiraIssueKey Summary",
        "",
        "- Comment",
        "```",
        "",
        "## Notes",
        "",
      }
      vim.api.nvim_buf_set_lines(0, 0, 0, false, lines)
    end
  end

  -- Function to find files with the current notebook in the title
  function M.find_files()
    Snacks.picker.files({
      cwd = vim.fn.expand(nb_dir .. current_notebook),
      finder = "files",
      format = "file",
      hidden = false,
      ignored = false,
      follow = false,
      supports_live = true,
    })
  end

  -- Function to live grep with the current notebook in the title
  function M.live_grep()
    Snacks.picker.grep({
      cwd = vim.fn.expand(nb_dir .. current_notebook),
      supports_live = true,
      finder = "grep",
      format = "file",
      live = true,
    })
  end

  -- Function to find by tags
  function M.find_tags()
    Snacks.picker.grep({
      cwd = vim.fn.expand(nb_dir .. current_notebook),
      live = true,
      supports_live = true,
      finder = "grep",
      format = "file",
      search = "#",
    })
    -- local builtin = require("telescope.builtin")
    -- builtin.grep_string({
    --   search_dirs = { vim.fn.expand(nb_dir .. current_notebook) },
    --   default_text = "#",
    --   prompt_title = "Browse Tags in " .. current_notebook,
    --   additional_args = function()
    --     return { [[#\w+]] }
    --   end,
    -- })
  end

  function M.register_worklog()
    local script = "python3 notes.py worklog"
    require("snacks.terminal").open(script, {
      interactive = true,
      cwd = work_notes,
    })
  end

  function M.generate_report()
    local script = 'python3 notes.py report; read -n 1 -r -p "Press CR key to continue..."'
    require("snacks.terminal").open(script, {
      interactive = true,
      cwd = work_notes,
    })
  end

  function M.export_to_html()
    local css = vim.fn.expand("$HOME/.config/nvim/lua/custom/one-dark.css")
    local command = "pandoc -s -o "
      .. vim.fn.expand("%:r")
      .. ".html "
      .. vim.fn.expand("%")
      .. " --highlight-style=zenburn"
      .. " --css "
      .. css
    vim.cmd("! " .. command)
  end

  function M.open_weekly_entry(offset)
    offset = offset or 0
    local current_timestamp = os.time() + (offset * 7 * 86400)
    local current_date = os.date("*t", current_timestamp)
    local week_number = os.date("%U", current_timestamp) + 1
    -- Calculate the start and end dates of the week
    local start_of_week =
      os.time({ year = current_date.year, month = current_date.month, day = current_date.day - current_date.wday + 2 })
    local end_of_week =
      os.time({ year = current_date.year, month = current_date.month, day = current_date.day - current_date.wday + 6 })
    local start_date = os.date("%d %b", start_of_week)
    local end_date = os.date("%d %b", end_of_week)
    local new_date = string.format("WEEK %d (%s - %s)", week_number, start_date, end_date)
    local file = string.format("%d-W%02d", current_date.year, week_number)
    local dir = string.format(nb_dir .. current_notebook .. "/Weekly/%d/", current_date.year)
    local expanded_dir = vim.fn.expand(dir)

    os.execute("mkdir -p " .. expanded_dir)

    local file_path = expanded_dir .. file .. ".md"
    local title = os.date("%A, %B %d, %Y", current_timestamp) .. string.format(", Sprint Week %s", week_number)

    vim.cmd("edit " .. file_path)

    if vim.fn.line("$") == 1 then
      local lines = {
        "---",
        "title: '" .. title .. "'",
        "date: '" .. new_date .. "'",
        "tags: ['weekly']",
        "---",
        "",
        "#unlogged",
        "",
        "#weekly",
        "",
        "> [!note] Format",
        ">",
        "> - Status:",
        ">   - `Not Started`: Task is planned but no work has begun",
        ">   - `In Progress`: Work is ongoins and proceedins as expected",
        ">   - `Delayed`: Work is ongoing but behind schedule or facing issues",
        ">   - `Done`: Task is finished successfully",
        ">   - `Blocked`: Task cannot proceed due to an obstacle or dependency",
        "> - Expected date for completion",
        "> - Risks & Blockers: Description about the potential problems identified or blockers that may put the execution in risk.",
        "> - Comments: Notes about important aspect of the execution, like the progress of the task, decisions taken like scope reduction, new requirements, etc.",
        "",
        "# " .. new_date,
        "",
      }
      vim.api.nvim_buf_set_lines(0, 0, 0, false, lines)
    end
  end

  return M
end

local notebook = notebook_utils()
local keymap = vim.keymap
keymap.set("n", "<leader>n", "", { desc = "Notebook Journal" })
keymap.set(
  "n",
  "<leader>nj",
  notebook.open_journal_entry,
  { noremap = true, silent = true, desc = "Open today's journal entry" }
)
keymap.set("n", "<leader>np", function()
  notebook.open_journal_entry(-1)
end, { noremap = true, silent = true, desc = "Open yesterday's journal entry" })
keymap.set("n", "<leader>nn", function()
  notebook.open_journal_entry(1)
end, { noremap = true, silent = true, desc = "Open tomorrow's journal entry" })
keymap.set("n", "<leader>nf", notebook.find_files, { desc = "Find in Notebook" })
keymap.set("n", "<leader>nt", notebook.find_tags, { desc = "Search Tags" })
keymap.set("n", "<leader>ns", notebook.live_grep, { desc = "Search in Notebook" })
keymap.set("n", "<leader>nc", notebook.set_notebook, { desc = "Change Notebook" })
keymap.set("n", "<leader>nw", notebook.register_worklog, { desc = "Register Worklog" })
keymap.set("n", "<leader>nr", notebook.generate_report, { desc = "Generate Report" })
keymap.set("n", "<leader>nH", notebook.export_to_html, { desc = "Export to HTML" })
keymap.set("n", "<leader>nW", notebook.open_weekly_entry, { desc = "Open Weekly Entry" })
keymap.set("n", "<leader>nP", function()
  notebook.open_weekly_entry(-1)
end, { desc = "Open Previous Weekly Entry" })
