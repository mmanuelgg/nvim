local doom_one_plugins = {
  "neorg",
  "barbar",
  "telescope",
  "neogit",
  "nvim_tree",
  "dashboard",
  "startify",
  "whichkey",
  "indent_blankline",
  "vim_illuminate",
  "lspsaga",
  "gitgutter",
  "gitblame",
  "gitsigns",
  "diffview",
  "lightbulb",
  "floaterm",
  "fterm",
  "zen_mode",
  "auto_session",
  "auto_save",
  "toggleterm",
  "dap",
  "dapui",
  "dapinstall",
  "daprepl",
  "dapdap",
}
return {
  {
    "folke/tokyonight.nvim",
    -- lazy = false,
    -- opts = { style = "moon" },
  },
  {
    "catppuccin/nvim",
    name = "catppuccin",
    lazy = false,
  },
  {
    "rebelot/kanagawa.nvim",
    config = function()
      -- Default options:
      require("kanagawa").setup({
        compile = false, -- enable compiling the colorscheme
        undercurl = true, -- enable undercurls
        commentStyle = { italic = true },
        functionStyle = {},
        keywordStyle = { italic = true },
        statementStyle = { bold = true },
        typeStyle = {},
        transparent = false, -- do not set background color
        dimInactive = false, -- dim inactive window `:h hl-NormalNC`
        terminalColors = true, -- define vim.g.terminal_color_{0,17}
        colors = { -- add/modify theme and palette colors
          palette = {},
          theme = { wave = {}, lotus = {}, dragon = {}, all = {} },
        },
        overrides = function(colors) -- add/modify highlights
          return {}
        end,
        theme = "wave", -- Load "wave" theme when 'background' option is not set
        background = { -- map the value of 'background' option to a theme
          dark = "wave", -- try "dragon" !
          light = "lotus",
        },
      })
    end,
  },
  {
    "sainnhe/gruvbox-material",
    config = function()
      -- vim.cmd("set background=light")
      vim.g.gruvbox_material_enable_italic = true
      vim.g.gruvbox_material_enable_bold = true
      vim.g.gruvbox_material_background = "soft" -- "hard", "medium", "soft"
      vim.g.gruvbox_material_better_performance = 1
    end,
  },
  -- {
  --   "sainnhe/everforest",
  -- },
  {
    "neanias/everforest-nvim",
    version = false,
    config = function()
      require("everforest").setup({
        ---Controls the "hardness" of the background. Options are "soft", "medium" or "hard".
        ---Default is "medium".
        background = "medium",
        ---How much of the background should be transparent. 2 will have more UI
        ---components be transparent (e.g. status line background)
        transparent_background_level = 0,
        ---Whether italics should be used for keywords and more.
        italics = false,
        ---Disable italic fonts for comments. Comments are in italics by default, set
        ---this to `true` to make them _not_ italic!
        disable_italic_comments = false,
        ---By default, the colour of the sign column background is the same as the as normal text
        ---background, but you can use a grey background by setting this to `"grey"`.
        sign_column_background = "none",
        ---The contrast of line numbers, indent lines, etc. Options are `"high"` or
        ---`"low"` (default).
        ui_contrast = "low",
        ---Dim inactive windows. Only works in Neovim. Can look a bit weird with Telescope.
        ---
        ---When this option is used in conjunction with show_eob set to `false`, the
        ---end of the buffer will only be hidden inside the active window. Inside
        ---inactive windows, the end of buffer filler characters will be visible in
        ---dimmed symbols. This is due to the way Vim and Neovim handle `EndOfBuffer`.
        dim_inactive_windows = false,
        ---Some plugins support highlighting error/warning/info/hint texts, by
        ---default these texts are only underlined, but you can use this option to
        ---also highlight the background of them.
        diagnostic_text_highlight = false,
        ---Which colour the diagnostic text should be. Options are `"grey"` or `"coloured"` (default)
        diagnostic_virtual_text = "coloured",
        ---Some plugins support highlighting error/warning/info/hint lines, but this
        ---feature is disabled by default in this colour scheme.
        diagnostic_line_highlight = false,
        ---By default, this color scheme won't colour the foreground of |spell|, instead
        ---colored under curls will be used. If you also want to colour the foreground,
        ---set this option to `true`.
        spell_foreground = false,
        ---Whether to show the EndOfBuffer highlight.
        show_eob = true,
        ---Style used to make floating windows stand out from other windows. `"bright"`
        ---makes the background of these windows lighter than |hl-Normal|, whereas
        ---`"dim"` makes it darker.
        ---
        ---Floating windows include for instance diagnostic pop-ups, scrollable
        ---documentation windows from completion engines, overlay windows from
        ---installers, etc.
        ---
        ---NB: This is only significant for dark backgrounds as the light palettes
        ---have the same colour for both values in the switch.
        float_style = "bright",
        ---Inlay hints are special markers that are displayed inline with the code to
        ---provide you with additional information. You can use this option to customize
        ---the background color of inlay hints.
        ---
        ---Options are `"none"` or `"dimmed"`.
        inlay_hints_background = "none",
        ---You can override specific highlights to use other groups or a hex colour.
        ---This function will be called with the highlights and colour palette tables.
        ---@param highlight_groups Highlights
        ---@param palette Palette
        on_highlights = function(highlight_groups, palette) end,
        ---You can override colours in the palette to use different hex colours.
        ---This function will be called once the base and background colours have
        ---been mixed on the palette.
        ---@param palette Palette
        colours_override = function(palette) end,
      })
    end,
  },
  {
    "JoosepAlviste/palenightfall.nvim",
  },
  {
    "AlexvZyl/nordic.nvim",
    lazy = false,
    priority = 1000,
    config = function()
      -- require("nordic").load()
    end,
  },
  {
    "EdenEast/nightfox.nvim",
  },
  {
    "navarasu/onedark.nvim",
    config = function()
      require("onedark").setup({
        style = "cool",
        transparent = false,
        term_colors = true,

        toggle_style_key = "<leader>tc",
        toggle_style_list = { "dark", "cool", "warm", "light" }, -- { "dark", "darker", "cool", "deep", "warm", "warmer", "light" },

        code_style = {
          comments = "italic",
          keywords = "none",
          functions = "italic",
          variables = "none",
          strings = "italic",
        },

        lualine = {
          transparent = false,
        },

        colors = {
          light_purple = "#a9a1e1",
          base0 = "#1b2229",
          base1 = "#1c1e1e",
          base2 = "#202328",
          base3 = "#23272e",
          base4 = "#3f444a",
          base5 = "#5b6268",
          base6 = "#73797e",
          base7 = "#9ca0a4",
          base8 = "#dfdfdf",
        },
        highlights = {
          ["@variable.parameter"] = { fg = "$light_grey" },
          ["@variable.builtin"] = { fg = "$light_purple", fmt = "italic,bold" },
          ["@function"] = { fg = "$purple", fmt = "italic" },
          ["@function.method"] = { fg = "$purple", fmt = "italic" },
          ["@function.builtin"] = { fg = "$purple", fmt = "italic,bold" },
          ["@keyword"] = { fg = "$blue" },
          ["@operator"] = { fg = "$blue" },
          -- ["@keyword.conditional"] = { fg = "$blue" },
          ["@keyword.operator"] = { fg = "$blue" },
          -- ["@keyword.import"] = { fg = "$blue" },
          ["@keyword.function"] = { fg = "$blue" },
          -- ["@keyword.repeat"] = { fg = "$blue" },
          -- ["@keyword.exception"] = { fg = "$blue" },
          ["@constant"] = { fg = "$light_purple", fmt = "bold,italic" },
          ["@constructor"] = { fg = "$bg_blue" },
          ["@punctuation.bracket"] = { fg = "$blue" },
          ["@punctuation.delimiter"] = { fg = "$blue" },
          ["@string"] = { fg = "$green", fmt = "italic" },
          ["@lsp.type.property"] = { fg = "$purple" },
          ["@lsp.type.variable"] = { fg = "$base8" },
          ["LineNrAbove"] = { fg = "$light_purple" },
          ["CursorLineNr"] = { fg = "$orange" },
          ["DashboardFooter"] = { fg = "$light_purple" },
          ["DashboardHeader"] = { fg = "$green" },
          ["DashboardIcon"] = { fg = "$blue" },
          ["DashboardDesc"] = { fg = "$fg" },
          ["DashboardKey"] = { fg = "$orange" },
          ["LspReferenceRead"] = { bg = "$bg2", fmt = "underline" }, -- bg = "$bg_d"?
          ["LspReferenceWrite"] = { bg = "$bg2", fmt = "underline" },
          ["LspReferenceText"] = { bg = "$bg2", fmt = "underline" },
          ["@lsp.typemod.variable.readonly.javascript"] = { fg = "$light_purple", fmt = "bold,italic" },
          ["@lsp.type.modifier.java"] = { fg = "$purple" },
          ["@lsp.type.property.java"] = { fg = "$red" },
          ["@lsp.type.variable.java"] = { fg = "$cyan" },
          ["@lsp.type.parameter.java"] = { fg = "$dark_red" },
          -- ["@lsp.type.namespace.java"] = { fg = "$light_grey" },
        },

        diagnostics = {
          darker = true,
          undercurl = true,
          background = true,
        },
      })
      local custom_onedark = require("lualine.themes.onedark")
      custom_onedark.normal.a.bg = "#a9a1e1"
      -- custom_onedark.normal.a.bg = "#c678dd"
      custom_onedark.insert.a.bg = "#98c379"
      custom_onedark.visual.a.bg = "#61afef"
      custom_onedark.visual.a.gui = "bold"
      -- require("lualine").setup({
      --   options = {
      --     theme = custom_onedark,
      --   },
      -- })
    end,
  },
  -- {
  --   "NTBBloodbath/doom-one.nvim",
  --   config = function()
  --     -- Add color to cursor
  --     vim.g.doom_one_cursor_coloring = true
  --     -- Set :terminal colors
  --     vim.g.doom_one_terminal_colors = true
  --     -- Enable italic comments
  --     vim.g.doom_one_italic_comments = true
  --     -- Enable TS support
  --     vim.g.doom_one_enable_treesitter = true
  --     -- Color whole diagnostic text or only underline
  --     vim.g.doom_one_diagnostics_text_color = true
  --     -- Enable transparent background
  --     -- vim.g.doom_one_transparent_background = false
  --
  --     -- Pumblend transparency
  --     -- vim.g.doom_one_pumblend_enable = false
  --     -- vim.g.doom_one_pumblend_transparency = 20
  --
  --     for _, plugin in ipairs(doom_one_plugins) do
  --       vim.g["doom_one_plugin_" .. plugin] = true
  --     end
  --   end,
  -- },
  {
    "LazyVim/LazyVim",
    opts = {
      -- colorscheme = "default",
      -- colorscheme = "doom-one",
      colorscheme = "onedark",
      -- colorscheme = "palenightfall",
      -- colorscheme = "tokyonight-storm",
      -- colorscheme = "catppuccin-frappe",
    },
  },
}
