return {
  -- {
  --   "ThePrimeagen/harpoon",
  --   config = function()
  --     require("telescope").load_extension("harpoon")
  --     require("harpoon").setup({
  --       global_settings = {
  --         -- sets the marks upon calling `toggle` on the ui, instead of require `:w`.
  --         save_on_toggle = false,
  --
  --         -- saves the harpoon file upon every change. disabling is unrecommended.
  --         save_on_change = true,
  --
  --         -- sets harpoon to run the command immediately as it's passed to the terminal when calling `sendCommand`.
  --         enter_on_sendcmd = false,
  --
  --         -- closes any tmux windows harpoon that harpoon creates when you close Neovim.
  --         tmux_autoclose_windows = false,
  --
  --         -- filetypes that you want to prevent from adding to the harpoon list menu.
  --         excluded_filetypes = { "harpoon" },
  --
  --         -- set marks specific to each git branch inside git repository
  --         mark_branch = false,
  --
  --         -- enable tabline with harpoon marks
  --         tabline = false,
  --         tabline_prefix = "   ",
  --         tabline_suffix = "   ",
  --       },
  --     })
  --   end,
  --   keys = {
  --     { "<leader>h", "", desc = "Harpoon" },
  --     { "<leader>ha", "<cmd>lua require('harpoon.mark').add_file()<CR>", desc = "Harpoon: Add mark" },
  --     { "<leader>hm", "<cmd>lua require('harpoon.mark').toggle_file()<CR>", desc = "Harpoon: Toggle mark" },
  --     { "<leader>hh", "<cmd>lua require('harpoon.ui').toggle_quick_menu()<CR>", desc = "Harpoon: Toggle menu" },
  --     { "<leader>hn", "<cmd>lua require('harpoon.ui').nav_next()<CR>", desc = "Harpoon: Next" },
  --     { "<leader>hp", "<cmd>lua require('harpoon.ui').nav_prev()<CR>", desc = "Harpoon: Previous" },
  --     { "<leader>hj", "<cmd>lua require('harpoon.ui').nav_file(1)<CR>", desc = "Harpoon: Jump to file" },
  --     { "<leader>hk", "<cmd>lua require('harpoon.ui').nav_file(2)<CR>", desc = "Harpoon: Jump to file" },
  --     { "<leader>hl", "<cmd>lua require('harpoon.ui').nav_file(3)<CR>", desc = "Harpoon: Jump to file" },
  --     { "<leader>ht", "<cmd>lua require('harpoon.term').gotoTerminal(1)<CR>", desc = "Harpoon: Terminal 1" },
  --     { "<leader>hR", "<cmd>lua require('harpoon.mark').clear_all()<CR>", desc = "Harpoon: Clear all" },
  --   },
  -- },
  {
    "ThePrimeagen/harpoon",
    branch = "harpoon2",
    opts = {
      menu = {
        width = vim.api.nvim_win_get_width(0) - 4,
      },
      settings = {
        save_on_toggle = true,
      },
    },
    keys = function()
      local keys = {
        { "<leader>h", "", desc = "Harpoon" },
        {
          "<leader>ha",
          function()
            require("harpoon"):list():add()
          end,
          desc = "Add File",
        },
        {
          "<leader>hh",
          function()
            local harpoon = require("harpoon")
            harpoon.ui:toggle_quick_menu(harpoon:list())
          end,
          desc = "Quick Menu",
        },
        {
          "<leader>hc",
          function()
            require("harpoon"):list():clear()
          end,
          desc = "Clear",
        },
        {
          "<leader>hn",
          function()
            require("harpoon"):list():next()
          end,
          desc = "Next",
        },
        {
          "<leader>hp",
          function()
            require("harpoon"):list():prev()
          end,
          desc = "Previous",
        },
      }

      return keys
    end,
  },
}
