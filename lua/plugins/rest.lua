return {
  {
    "oysandvik94/curl.nvim",
    cmd = { "CurlOpen" },
    dependencies = { "nvim-lua/plenary.nvim" },
    config = function()
      require("curl").setup({
        open_with = "split", -- "tab"/"split"
      })
    end,
    keys = {
      {
        "<leader>r",
        "",
        desc = "Rest (curl)",
      },
      {
        "<leader>rc",
        "<CMD>lua require('curl').open_curl_tab()<CR>",
        desc = "Current Scope",
      },
      {
        "<leader>ro",
        "<CMD>lua require('curl').open_global_tab()<CR>",
        desc = "Global",
      },
      {
        "<leader>rq",
        "<CMD>CurlClose<CR>",
        desc = "Close",
      },
      {
        "<leader>rS",
        "<CMD>lua require('curl').create_scoped_collection()<CR>",
        desc = "Create scoped collection",
      },
      {
        "<leader>rG",
        "<CMD>lua require('curl').create_global_collection()<CR>",
        desc = "Create global collection",
      },
      {
        "<leader>rs",
        "<CMD>lua require('curl').pick_scoped_collection()<CR>",
        desc = "Pick scoped collection",
      },
      {
        "<leader>rg",
        "<CMD>lua require('curl').pick_global_collection()<CR>",
        desc = "Pick global collection",
      },
    },
  },
}
