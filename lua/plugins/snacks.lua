-- Define Doom-One colors
local colors = {
  red = "#ff6c6b",
  yellow = "#ecbe7b",
  blue = "#51afef",
  orange = "#da8548",
  green = "#98be65",
  violet = "#c678dd",
  cyan = "#008080",
}

-- Create highlight groups
vim.api.nvim_set_hl(0, "SnacksIndent1", { fg = colors.red, nocombine = true })
vim.api.nvim_set_hl(0, "SnacksIndent2", { fg = colors.yellow, nocombine = true })
vim.api.nvim_set_hl(0, "SnacksIndent3", { fg = colors.blue, nocombine = true })
vim.api.nvim_set_hl(0, "SnacksIndent4", { fg = colors.orange, nocombine = true })
vim.api.nvim_set_hl(0, "SnacksIndent5", { fg = colors.green, nocombine = true })
vim.api.nvim_set_hl(0, "SnacksIndent6", { fg = colors.violet, nocombine = true })
vim.api.nvim_set_hl(0, "SnacksIndent7", { fg = colors.cyan, nocombine = true })
local snacks_highlight = {
  "SnacksIndent1",
  "SnacksIndent2",
  "SnacksIndent3",
  "SnacksIndent4",
  "SnacksIndent5",
  "SnacksIndent6",
  "SnacksIndent7",
}
-- vim.g.rainbow_delimiters = { highlight = snacks_highlight }
return {
  {
    "folke/snacks.nvim",
    priority = 1000,
    lazy = false,
    ---@type snacks.Config
    opts = {
      bigfile = { enabled = true },
      dashboard = { enabled = true },
      indent = {
        -- Disable for now, cannot get colors to work properly
        enabled = true,
        scope = {
          enabled = true,
          underline = true,
          hl = snacks_highlight,
        },
        -- chunk = {
        --   enabled = true,
        --   hl = snacks_highlight,
        -- },
      },
      input = { enabled = true },
      notifier = {
        enabled = true,
        timeout = 3000,
      },
      quickfile = { enabled = true },
      scope = { enabled = true },
      scroll = { enabled = false },
      statuscolumn = { enabled = true },
      words = { enabled = true },
      styles = {
        notification = {
          relative = "editor",
          -- wo = { wrap = true } -- Wrap notifications
        },
      },
      picker = {
        sources = {
          explorer = {
            layout = {
              layout = {
                position = "right",
              },
            },
          },
        },
      },
      explorer = { enabled = true },
      image = {},
    },
    keys = {
      {
        "<leader><enter>",
        function()
          Snacks.explorer()
        end,
        desc = "Explorer",
      },
      {
        "<leader>e",
        function()
          Snacks.explorer()
        end,
        desc = "Explorer",
      },
      {
        "<leader><space>",
        function()
          Snacks.picker.files({ hidden = true, ignored = true })
        end,
        desc = "Files",
      },
      {
        "<leader>fs",
        function()
          Snacks.picker.smart()
        end,
        desc = "Smart",
      },
      {
        "<leader>z",
        function()
          Snacks.zen()
        end,
        desc = "Toggle Zen Mode",
      },
      {
        "<leader>Z",
        function()
          Snacks.zen.zoom()
        end,
        desc = "Toggle Zoom",
      },
      {
        "<leader>.",
        function()
          Snacks.scratch()
        end,
        desc = "Toggle Scratch Buffer",
      },
      {
        "<leader>S",
        function()
          Snacks.scratch.select()
        end,
        desc = "Select Scratch Buffer",
      },
      {
        "<leader>sN",
        function()
          Snacks.notifier.show_history()
        end,
        desc = "Notification History",
      },
      {
        "<leader>bd",
        function()
          Snacks.bufdelete()
        end,
        desc = "Delete Buffer",
      },
      {
        "<leader>cR",
        function()
          Snacks.rename.rename_file()
        end,
        desc = "Rename File",
      },
      {
        "<leader>gB",
        function()
          Snacks.gitbrowse()
        end,
        desc = "Git Browse",
      },
      {
        "<leader>gb",
        function()
          Snacks.git.blame_line()
        end,
        desc = "Git Blame Line",
      },
      {
        "<leader>gf",
        function()
          Snacks.lazygit.log_file()
        end,
        desc = "Lazygit Current File History",
      },
      {
        "<leader>gg",
        function()
          Snacks.lazygit()
        end,
        desc = "Lazygit",
      },
      {
        "<leader>gl",
        function()
          Snacks.lazygit.log()
        end,
        desc = "Lazygit Log (cwd)",
      },
      {
        "<leader>un",
        function()
          Snacks.notifier.hide()
        end,
        desc = "Dismiss All Notifications",
      },
      {
        "<c-/>",
        function()
          Snacks.terminal()
        end,
        desc = "Toggle Terminal",
      },
      {
        "<c-_>",
        function()
          Snacks.terminal()
        end,
        desc = "which_key_ignore",
      },
      {
        "]]",
        function()
          Snacks.words.jump(vim.v.count1)
        end,
        desc = "Next Reference",
        mode = { "n", "t" },
      },
      {
        "[[",
        function()
          Snacks.words.jump(-vim.v.count1)
        end,
        desc = "Prev Reference",
        mode = { "n", "t" },
      },
      {
        "<leader>si",
        function()
          Snacks.picker.icons()
        end,
        desc = "Search icons/emojis",
      },
      {
        "g/",
        function()
          Snacks.picker.grep_word()
        end,
        desc = "Search Current Word Under Cursor",
      },
      {
        "<leader>D",
        function()
          Snacks.terminal.open("lazydocker")
        end,
        desc = "Lazydocker",
      },
    },
    init = function()
      vim.api.nvim_create_autocmd("User", {
        pattern = "VeryLazy",
        callback = function()
          -- Setup some globals for debugging (lazy-loaded)
          _G.dd = function(...)
            Snacks.debug.inspect(...)
          end
          _G.bt = function()
            Snacks.debug.backtrace()
          end
          vim.print = _G.dd -- Override print to use snacks for `:=` command

          -- Create some toggle mappings
          Snacks.toggle.option("spell", { name = "Spelling" }):map("<leader>us")
          Snacks.toggle.option("wrap", { name = "Wrap" }):map("<leader>uw")
          Snacks.toggle.option("relativenumber", { name = "Relative Number" }):map("<leader>uL")
          Snacks.toggle.diagnostics():map("<leader>ud")
          Snacks.toggle.line_number():map("<leader>ul")
          Snacks.toggle
            .option("conceallevel", { off = 0, on = vim.o.conceallevel > 0 and vim.o.conceallevel or 2 })
            :map("<leader>uc")
          Snacks.toggle.treesitter():map("<leader>uT")
          Snacks.toggle.option("background", { off = "light", on = "dark", name = "Dark Background" }):map("<leader>ub")
          Snacks.toggle.inlay_hints():map("<leader>uh")
          Snacks.toggle.indent():map("<leader>ug")
          Snacks.toggle.dim():map("<leader>uD")
        end,
      })
      vim.api.nvim_create_autocmd("BufEnter", {
        pattern = "*.md",
        callback = function()
          vim.b.snacks_indent_was_enabled = Snacks.indent.enabled
          Snacks.indent.disable()
        end,
      })
      vim.api.nvim_create_autocmd("BufLeave", {
        pattern = "*.md",
        callback = function()
          if vim.b.snacks_indent_was_enabled then
            Snacks.indent.enable()
          end
        end,
      })
    end,
  },
}
