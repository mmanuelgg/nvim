local icons = require("lazyvim.config").icons
return {
  {
    "folke/noice.nvim",
    opts = function(_, opts)
      table.insert(opts.routes, {
        filter = {
          event = "notify",
          find = "No information available",
        },
        opts = { skip = true },
      })
      opts.presets.lsp_doc_border = true
    end,
  },
  {
    "nvim-lualine/lualine.nvim",
    config = {
      sections = {
        lualine_c = {
          {
            "diagnostics",
            symbols = {
              error = icons.diagnostics.Error,
              warn = icons.diagnostics.Warn,
              info = icons.diagnostics.Info,
              hint = icons.diagnostics.Hint,
            },
          },
          { "filetype", icon_only = true, separator = "", padding = { left = 1, right = 0 } },
          { "filename", path = 1, symbols = { modified = "  ", readonly = "", unnamed = "" } },
        },
      },
    },
  },
  {
    "akinsho/bufferline.nvim",
    event = "VeryLazy",
    dependencies = { "nvim-tree/nvim-web-devicons" },
    keys = {
      { "<leader>bp", "<Cmd>BufferLineTogglePin<CR>", desc = "Toggle Pin" },
      { "<leader>bP", "<Cmd>BufferLineGroupClose ungrouped<CR>", desc = "Delete Non-Pinned Buffers" },
      { "<leader>bo", "<Cmd>BufferLineCloseOthers<CR>", desc = "Delete Other Buffers" },
      { "<leader>br", "<Cmd>BufferLineCloseRight<CR>", desc = "Delete Buffers to the Right" },
      { "<leader>bl", "<Cmd>BufferLineCloseLeft<CR>", desc = "Delete Buffers to the Left" },
      { "<leader>bH", "<cmd>BufferLineMovePrev<cr>", desc = "Move buffer prev" },
      { "<leader>bL", "<cmd>BufferLineMoveNext<cr>", desc = "Move buffer next" },
      { "<leader>bs", "<cmd>BufferLinePick<cr>", desc = "Select buffer" },
      { "<leader>bS", "<cmd>BufferLinePickClose<cr>", desc = "Select buffer to close" },
      { "<S-h>", "<cmd>BufferLineCyclePrev<cr>", desc = "Prev Buffer" },
      { "<S-l>", "<cmd>BufferLineCycleNext<cr>", desc = "Next Buffer" },
      { "[b", "<cmd>BufferLineCyclePrev<cr>", desc = "Prev Buffer" },
      { "]b", "<cmd>BufferLineCycleNext<cr>", desc = "Next Buffer" },
      { "[B", "<cmd>BufferLineMovePrev<cr>", desc = "Move buffer prev" },
      { "]B", "<cmd>BufferLineMoveNext<cr>", desc = "Move buffer next" },
    },
    opts = {
      options = {
      -- stylua: ignore
      close_command = function(n) Snacks.bufdelete(n) end,
      -- stylua: ignore
      right_mouse_command = function(n) Snacks.bufdelete(n) end,
        diagnostics = "nvim_lsp",
        always_show_bufferline = false,
        diagnostics_indicator = function(_, _, diag)
          local icons = LazyVim.config.icons.diagnostics
          local ret = (diag.error and icons.Error .. diag.error .. " " or "")
            .. (diag.warning and icons.Warn .. diag.warning or "")
          return vim.trim(ret)
        end,
        hover = {
          enabled = true,
          delay = 200,
          reveal = { "close" },
        },
        offsets = {
          {
            filetype = "neo-tree",
            text = "Neo-tree",
            highlight = "Directory",
            text_align = "left",
          },
        },
        ---@param opts bufferline.IconFetcherOpts
        get_element_icon = function(opts)
          return LazyVim.config.icons.ft[opts.filetype]
        end,
      },
    },
    config = function(_, opts)
      require("bufferline").setup(opts)
      -- Fix bufferline when restoring a session
      vim.api.nvim_create_autocmd({ "BufAdd", "BufDelete" }, {
        callback = function()
          vim.schedule(function()
            pcall(nvim_bufferline)
          end)
        end,
      })
    end,
  },
  {
    "nvim-neo-tree/neo-tree.nvim",
    enabled = false,
    keys = {
      {
        "<leader>e",
        -- "<cmd>Neotree reveal toggle<cr>",
        "<cmd>Neotree reveal_force_cwd toggle<cr>",
        desc = "NeoTree Toggle",
      },
    },
    config = function()
      require("neo-tree").setup({
        window = { position = "right" },
        default_component_configs = {
          indent = {
            with_expanders = true,
            expander_collapsed = "",
            expander_expanded = "",
            expander_highlight = "NeoTreeExpander",
          },
        },
      })
    end,
    init = false,
  },
  {
    "HiPhish/rainbow-delimiters.nvim",
  },
  {
    "laytan/cloak.nvim",
    config = function()
      require("cloak").setup({
        require("cloak").setup({
          enabled = true,
          cloak_character = "*",
          -- The applied highlight group (colors) on the cloaking, see `:h highlight`.
          highlight_group = "Comment",
          -- Applies the length of the replacement characters for all matched
          -- patterns, defaults to the length of the matched pattern.
          cloak_length = nil, -- Provide a number if you want to hide the true length of the value.
          -- Whether it should try every pattern to find the best fit or stop after the first.
          try_all_patterns = true,
          -- Set to true to cloak Telescope preview buffers. (Required feature not in 0.1.x)
          cloak_telescope = true,
          -- Re-enable cloak when a matched buffer leaves the window.
          cloak_on_leave = false,
          patterns = {
            {
              -- Match any file starting with '.env'.
              -- This can be a table to match multiple file patterns.
              file_pattern = ".env*",
              -- Match an equals sign and any character after it.
              -- This can also be a table of patterns to cloak,
              -- example: cloak_pattern = { ':.+', '-.+' } for yaml files.
              cloak_pattern = "=.+",
              -- A function, table or string to generate the replacement.
              -- The actual replacement will contain the 'cloak_character'
              -- where it doesn't cover the original text.
              -- If left empty the legacy behavior of keeping the first character is retained.
              replace = nil,
            },
          },
        }),
      })
    end,
  },
  -- For nvchad plugins
  { "nvchad/volt", lazy = true },
  {
    "nvchad/minty",
    lazy = true,
    keys = {
      { "<leader>uH", '<cmd>lua require("minty.huefy").open()<cr>', desc = "Toggle Minty Huefy" },
      { "<leader>uS", '<cmd>lua require("minty.shades").open()<cr>', desc = "Toggle Minty Shades" },
    },
  },
}
