local ls = require("luasnip")
local s = ls.snippet
local t = ls.text_node
local i = ls.insert_node

ls.add_snippets("markdown", {
  s("frontmatter", {
    t({ "---", "title: " .. os.date("%A, %B %d, %Y"), "date: " .. os.date("%Y-%m-%d"), "tags: ['" }),
    i(1, "tags"),
    t({ "']", "---", "" }),
  }),
  s("planned", {
    t({ "```markdown", "Planned for today:", "", "" }),
    i(1, "JiraIssue"),
    t({ " " }),
    i(2, "Title"),
    t({ "", "- " }),
    i(3, "Comment"),
    t({ "", "```", "", "## Notes", "" }),
  }),
  s("log", {
    t({ "### `" }),
    i(1, "JiraIssue"),
    t({ "` `" }),
    i(2, "Title"),
    t({ "`", "", "> `PLANNED` `" }),
    i(3, "HH"),
    t({ ":" }),
    i(4, "MM"),
    t({ " > " }),
    i(5, "HH"),
    t({ ":" }),
    i(6, "MM"),
    t({ "`", "> - " }),
    i(7, "Comment"),
    t({ "", "" }),
  }),
})
