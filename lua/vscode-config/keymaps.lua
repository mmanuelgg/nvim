vim.api.nvim_set_keymap("n", "<C-a>", "ggVG", { noremap = true, silent = true })
vim.api.nvim_set_keymap("n", "<Esc>", "<Esc>:noh<CR>", { noremap = true, silent = true })

function CallVscodeSpace()
  vim.fn["VSCodeCall"]("vspacecode.space")
end

function CallVscodeComma()
  vim.fn["VSCodeCall"]("vspacecode.space")
  vim.fn["VSCodeCall"]("whichkey.triggerKey", "m")
end

function CallVscodeComment()
  vim.fn["VSCodeCall"]("vspacecode.space")
  vim.fn["VSCodeCall"]("whichkey.triggerKey", ";")
end

vim.api.nvim_set_keymap("n", "<space>", ":lua CallVscodeSpace()<CR>", { noremap = true, silent = true })
vim.api.nvim_set_keymap("v", "<space>", ":lua CallVscodeSpace()<CR>", { noremap = true, silent = true })
vim.api.nvim_set_keymap("n", ",", ":lua CallVscodeComma()<CR>", { noremap = true, silent = true })
vim.api.nvim_set_keymap("v", ";", ":lua CallVscodeComment()<CR>", { noremap = true, silent = true })
